/*-
 * Copyright (c) 2020 Shawn Webb <shawn.webb@hardenedbsd.org>
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer
 *    in this position and unchanged.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR(S) ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR(S) BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include <archive.h>
#include <archive_entry.h>
#include <assert.h>
#include <fcntl.h>
#include <fts.h>
#include <string.h>
#include <pwd.h>
#include <grp.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/extattr.h>

#include "pkg.h"
#include "private/event.h"
#include "private/pkg.h"

int
pkg_archive_extattrs(int fd, struct archive_entry *entry)
{
	const char *nameprefix = "system.";
	char *endp, *name, *names, *namep;
	ssize_t datasize, listsize;
	int err, namespace;
	uint8_t namesize;
	void *attrdata;

	err = EPKG_OK;
	attrdata = NULL;
	names = NULL;

	namespace = EXTATTR_NAMESPACE_SYSTEM;

	listsize = extattr_list_fd(fd, namespace, NULL, 0);
	if (listsize < 0) {
		return (EPKG_OK);
	}

	if (listsize == 0) {
		return (EPKG_OK);
	}

	names = calloc(listsize, 1);
	if (names == NULL) {
		return (EPKG_OK);
	}

	if (extattr_list_fd(fd, namespace, names, listsize) !=
	    listsize) {
		goto end;
	}
	endp = names + (size_t)listsize;
	for (namep = names; namep < endp; namep += namesize) {
		namesize = *((uint8_t *)(namep));
		name = calloc(strlen(nameprefix) + namesize+1, 1);
		if (name == NULL) {
			goto end;
		}
		namep += sizeof(uint8_t);
		strcpy(name, nameprefix);
		strncat(name, namep, namesize);

		datasize = extattr_get_fd(fd, namespace, name+strlen(nameprefix), NULL, 0);
		if (datasize < 0) {
			free(name);
			continue;
		}

		attrdata = calloc(1, (size_t)datasize);
		if (attrdata == NULL) {
			free(name);
			goto end;
		}

		if (extattr_get_fd(fd, namespace, name+strlen(nameprefix), attrdata,
		    datasize) != datasize) {
			perror("extattr_get_fd");
			free(name);
			free(attrdata);
			goto end;
		}

		archive_entry_xattr_add_entry(entry, name, attrdata,
		    datasize);

		free(name);
		free(attrdata);
	}

end:
	free(names);
	return (EPKG_OK);
}
